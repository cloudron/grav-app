This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: Changeme123#<br/>

Please change the admin password immediately.

The admin interface is located at `/admin`.
