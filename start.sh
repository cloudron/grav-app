#!/bin/bash

set -eu

mkdir -p /app/data/backup \
         /run/apache2 \
         /run/sessions \
         /run/grav/assets \
         /run/grav/cache \
         /run/grav/images \
         /run/grav/logs \
         /run/grav/tmp \
         /run/grav/sessions

if [[ ! -d /app/data/user ]]; then
    echo "==> Initializing GravCMS on first run"

    echo "==> Copying default user directory"
    cp -r /app/code/user.original /app/data/user

    echo "==> Creating administrator"
    /app/code/bin/plugin login new-user -u admin -p 'Changeme123#' -e admin@example.com -P a --fullname Administrator --language=en
else
    # we used to update the user directory with the latest themes contents but not anymore. See https://forum.cloudron.io/topic/6319/logos-has-been-removed-after-app-restart . The plugin is still updated since this package provides admin plugin. If that's a problem, we can ignore that too.
    echo "==> Updating plugins"
    for plugin in `find /app/code/user.original/plugins/* -maxdepth 0 -type d -printf "%f\n"`; do
        echo "Update plugin: ${plugin}"
        rm -rf "/app/data/user/plugins/${plugin}"
        cp -rf "/app/code/user.original/plugins/${plugin}" "/app/data/user/plugins/${plugin}"
    done
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "=> Changing permissions"
chown -R www-data:www-data /app/data /run/grav

echo "==> Configuring Grav"
mkdir -p /app/data/user/config/plugins
[[ ! -f /app/data/user/config/plugins/email.yaml ]] && cp /app/data/user/plugins/email/email.yaml /app/data/user/config/plugins/email.yaml
yq eval -i ".from=\"${CLOUDRON_MAIL_FROM}\"" /app/data/user/config/plugins/email.yaml
yq eval -i ".from_name=\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-GravCMS}\"" /app/data/user/config/plugins/email.yaml
yq eval -i ".mailer.engine=\"smtp\"" /app/data/user/config/plugins/email.yaml
yq eval -i ".mailer.smtp.server=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" /app/data/user/config/plugins/email.yaml
yq eval -i ".mailer.smtp.port=${CLOUDRON_MAIL_SMTP_PORT}" /app/data/user/config/plugins/email.yaml
yq eval -i ".mailer.smtp.user=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" /app/data/user/config/plugins/email.yaml
yq eval -i ".mailer.smtp.password=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" /app/data/user/config/plugins/email.yaml

echo "==> Starting Grav CMS"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
