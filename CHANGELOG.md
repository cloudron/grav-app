[0.1.0]
* Initial version

[0.2.0]
* Fix admin creation

[0.3.0]
* Update GravCMS to 1.6.20

[1.0.0]
* Stable release

[1.0.1]
* Fix issue where startup script tries to re-initialize an existing grav install
* Add documentation URL

[1.0.2]
* Update GravCMS to 1.6.21

[1.0.3]
* Update GravCMS to 1.6.22

[1.0.4]
* Update GravCMS to 1.6.23

[1.1.0]
* Update Grav to 1.6.25
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.6.25)
* Use latest base image 2.0.0
* Update PHP to 7.3

[1.1.1]
* Update Grav to 1.6.26
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.6.26)

[1.1.2]
* Update Grav to 1.6.27
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.6.27)

[1.1.3]
* Update Grav to 1.6.28
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.6.28)

[1.1.4]
* Update Grav to 1.6.29
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.6.29)

[1.2.0]
* Update Grav to 1.6.31
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.6.31)

[1.3.0]
* Update Grav to 1.7.0
* [Full changelog](https://getgrav.org/blog/grav-17-stable-jan-2021)
* All new Flex-Pages support providing better scaling and a new UI for managing pages in the admin
* New per-language fallback options for multi-language
* Added a new built-in {% cache %} Twig tag to cache chunks of twig templates for increased performance
* Twig auto-escape being enabled by default (with backwards compatible logic to disable it when needed)
* Added several new Twig functions and filters
* Updated Symfony components to 4.4
* Various core code cleanup to fix many "PHP Standards" (phpstan) errors

[1.3.1]
* Update Grav to 1.7.3

[1.3.2]
* Log the X-Forward-For IP in the logs
* Change scheduler granularity to 1 second

[1.3.3]
* Update Grav to 1.7.5
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.5)

[1.4.0]
* Update Grav to 1.7.6
* Update base image to v3
* Update PHP to 7.4

[1.4.1]
* Update Grav to 1.7.7
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.7)

[1.4.2]
* Update Grav to 1.7.8
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.8)

[1.4.3]
* Update Grav to 1.7.9
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.9)

[1.4.4]
* Update Grav to 1.7.10
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.10)

[1.4.5]
* Update Grav to 1.7.12
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.12)

[1.4.6]
* Update Grav to 1.7.13
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.13)

[1.4.7]
* Update Grav to 1.7.14
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.14)

[1.4.8]
* Update Grav to 1.7.15
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.15)

[1.4.9]
* Update Grav to 1.7.16
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.16)

[1.4.10]
* Update Grav to 1.7.17
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.17)

[1.4.11]
* Update Grav to 1.7.18
* Fixed `Warning: Undefined array key "SERVER_SOFTWARE" in index.php` [#3408](https://github.com/getgrav/grav/issues/3408)
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.18)

[1.5.0]
* Update Grav to 1.7.20
* Update built-in themes and plugins on update
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.20)

[1.5.1]
* Update Grav to 1.7.21

[1.5.2]
* Update Grav to 1.7.22
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.22)

[1.5.3]
* Update Grav to 1.7.23
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.23)

[1.5.4]
* Update Grav to 1.7.24
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.24)

[1.5.5]
* Update Grav to 1.7.25
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.25)

[1.5.6]
* Update Grav to 1.7.26
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.26)

[1.5.7]
* Update Grav to 1.7.26.1
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.26.1)

[1.5.8]
* Remove hardcoded PHP memory limit in apache configs
* Do not overwrite themes on update and restart

[1.5.9]
* Update Grav to 1.7.27.1
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.27.1)

[1.5.10]
* Update Grav to 1.7.28
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.28)

[1.5.11]
* Update Grav to 1.7.29
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.29)

[1.5.12]
* Update Grav to 1.7.29.1
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.29.1)

[1.5.13]
* Update Grav to 1.7.30
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.30)

[1.5.14]
* Update Grav to 1.7.31
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.31)

[1.5.15]
* Update Grav to 1.7.32
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.32)

[1.5.16]
* Update Grav to 1.7.33
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.33)

[1.6.0]
* Update Grav to 1.7.34
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.34)
* Email display name support

[1.6.1]
* Update Grav to 1.7.35
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.35)

[1.6.2]
* Update Grav to 1.7.36
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.36)

[1.6.3]
* Update Grav to 1.7.37
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.37)

[1.6.4]
* Update Grav to 1.7.37.1
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.37.1)

[1.7.0]
* Update Grav to 1.7.38
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.38)
* Update base image to 4.0.0

[1.7.1]
* Updater Grav to 1.7.39
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.39)

[1.7.2]
* Update Grav to 1.7.39.2
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.39.2)
* Updated with quick fix for DebugBar broken styling

[1.7.3]
* Update Grav to 1.7.39.3
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.39.3)

[1.7.4]
* Update Grav to 1.7.39.4
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.39.4)

[1.7.5]
* Update Grav to 1.7.40
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.40)

[1.7.6]
* Update Grav to 1.7.41
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.41)

[1.7.7]
* Update Grav to 1.7.41.1
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.41.1)
* Fixed certain UTF-8 characters breaking `Truncator` class [#3716](https://github.com/getgrav/grav/issues/3716)

[1.7.8]
* Update Grav to 1.7.41.2
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.41.2)

[1.7.9]
* Update Grav to 1.7.42
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.42)

[1.7.10]
* Update Grav to 1.7.42.1
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.42.1)

[1.7.11]
* Update Grav to 1.7.42.2
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.42.2)

[1.7.12]
* Update Grav to 1.7.42.3
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.42.3)

[1.7.13]
* Update Grav to 1.7.43
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.43)

[1.7.14]
* Update Grav to 1.7.44
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.44)

[1.7.15]
* Update Grav to 1.7.45
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.45)

[1.7.16]
* Update Grav to 1.7.46
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.46)

[1.7.17]
* Update Grav to 1.7.47
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.47)

[1.7.18]
* Update Grav to 1.7.48
* [Full changelog](https://github.com/getgrav/grav/releases/tag/1.7.48)

