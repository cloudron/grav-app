FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=getgrav/grav versioning=semver
ARG GRAV_VERSION=1.7.48

# Clone and install grav
# https://learn.getgrav.org/16/basics/folder-structure
RUN curl -L https://github.com/getgrav/grav/archive/${GRAV_VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    chown -R www-data:www-data /app/code && \
    gosu www-data:www-data composer install --no-dev -o && \
    gosu www-data:www-data bin/grav install && \
    gosu www-data:www-data bin/gpm install admin && \
    rm -rf /app/code/assets && ln -sf /run/grav/assets /app/code/assets && \
    rm -rf /app/code/cache && ln -sf /run/grav/cache /app/code/cache && \
    rm -rf /app/code/images && ln -sf /run/grav/images /app/code/images && \
    rm -rf /app/code/logs && ln -sf /run/grav/logs /app/code/logs && \
    rm -rf /app/code/tmp && ln -sf /run/grav/tmp /app/code/tmp

RUN mv /app/code/user /app/code/user.original && \
    ln -s /app/data/user /app/code/user && \
    rm -rf /app/code/backup && ln -sf /app/data/backup /app/code/backup

# Configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/grav.conf /etc/apache2/sites-enabled/grav.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf
RUN a2enmod rewrite

# Configure mod_php
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/grav/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/8.1/apache2/php.ini /etc/php/8.1/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# Set up package scripts
COPY start.sh /app/pkg/

# Lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

RUN echo "alias grav='sudo -u www-data -i -- /app/code/bin/grav'" >> /root/.bashrc
RUN echo "alias gpm='sudo -u www-data -i -- /app/code/bin/gpm'" >> /root/.bashrc

CMD [ "/app/pkg/start.sh" ]
