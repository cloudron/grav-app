#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT) || 40000;
    const ADMIN_USERNAME = 'admin', ADMIN_PASSWORD = 'Changeme123#';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function adminLogin() {
        await browser.get('https://' + app.fqdn + '/admin');
        await browser.wait(until.elementLocated(By.xpath('//input[@placeholder="Username or Email"]'), TIMEOUT));
        await browser.findElement(By.xpath('//input[@placeholder="Username or Email"]')).sendKeys(ADMIN_USERNAME);
        await browser.findElement(By.xpath('//input[@placeholder="Password"]')).sendKeys(ADMIN_PASSWORD);
        await browser.findElement(By.xpath('//button[@value="login"]')).click();
        await browser.sleep(10000);
        await browser.wait(until.elementIsVisible(browser.findElement(By.id('admin-menu'))), TIMEOUT);
    }

    async function logout() {
        await browser.sleep(2000);
        const logoutLink = browser.findElement(By.xpath('//a[contains(@href, "/admin/task:logout")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', logoutLink);
        await logoutLink.click();
        await browser.sleep(2000);
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//div[contains(text(), "You have been logged out")]'))), TIMEOUT);
    }

    async function checkReport() {
        await browser.get('https://' + app.fqdn + '/admin/tools/reports');
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//strong[contains(text(), "No issues found...")]'))), TIMEOUT); // security
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', adminLogin);
    it('can check report', checkReport);
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can login', adminLogin);
    it('can check report', checkReport);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');

        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', adminLogin);
    it('can check report', checkReport);
    it('can logout', logout);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');

        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can login', adminLogin);
    it('can check report', checkReport);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id org.getgrav.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', adminLogin);
    it('can check report', checkReport);
    it('can logout', logout);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can login', adminLogin);
    it('can check report', checkReport);
    it('can logout', logout);
    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
